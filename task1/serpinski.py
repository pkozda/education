import turtle


def draw_triangle(my_turtle, points, color):
    """Draw triangle using points."""

    my_turtle.fillcolor(color)
    my_turtle.up()
    my_turtle.goto(points[0][0], points[0][1])
    my_turtle.down()
    my_turtle.begin_fill()
    my_turtle.goto(points[1][0], points[1][1])
    my_turtle.goto(points[2][0], points[2][1])
    my_turtle.goto(points[0][0], points[0][1])
    my_turtle.end_fill()


def middle(point1, point2):
    """Get new points in the middles of sides."""

    new_point1 = (point1[0] + point2[0]) / 2
    new_point2 = (point1[1] + point2[1]) / 2
    return new_point1, new_point2


def iterative(my_turtle, points, grade, color):
    """Iterative method of drawing Serpinski Triangle.
    This method using recursive function.

    """

    draw_triangle(my_turtle, points, color[grade])

    if grade > 0:
        new_grade = grade - 1
        iterative(my_turtle, [points[0], middle(points[0], points[1]),
                  middle(points[0], points[2])], new_grade, color)
        iterative(my_turtle, [points[1], middle(points[0], points[1]),
                  middle(points[1], points[2])], new_grade, color)
        iterative(my_turtle, [points[2], middle(points[2], points[1]),
                  middle(points[0], points[2])], new_grade, color)


def main():
    """Main function"""

    fred = turtle.Turtle()
    fred.fillcolor("black")
    fred.speed(0)
    wn = turtle.Screen()
    color = ["red", "green", "blue", "purple", "yellow"]
    points_list = [[-200, -100], [0, 200], [200, -100]]
    grade = int(input("Enter the desired grade of triangle: "))

    iterative(fred, points_list, grade, color)
    wn.exitonclick()

if __name__ == "__main__":
    main()
