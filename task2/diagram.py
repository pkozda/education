import turtle


def goto(my_turtle, x, y):
    """Moves turtle to x,y position without painting."""

    my_turtle.up()
    my_turtle.goto(x, y)
    my_turtle.down()


def circle(my_turtle, r):
    """Draw a circle with radius r"""

    my_turtle.circle(r)


def line(my_turtle, n, say):
    """Draw a line.
    Draw a line with length = 50 * n,
    where n is how many times word is in sentence.

    """

    for i in range(n):
        my_turtle.fd(50)
        circle(my_turtle, 3)
    my_turtle.up()
    my_turtle.fd(30)
    my_turtle.write(say)
    my_turtle.goto(0, 0)
    my_turtle.down()


def to_dict(my_str):
    """Transforms string into dictionary."""

    my_list = my_str.split(" ")
    my_dict = {}
    for i in range(len(my_list)):
        if my_list[i] not in my_dict:
            my_dict[my_list[i]] = 1
        else:
            my_dict[my_list[i]] += 1
    print my_dict
    return my_dict


def sector(my_turtle, n):
    """Draw a sector."""

    my_turtle.begin_fill()
    my_turtle.fd(100)
    my_turtle.left(90)
    my_turtle.circle(100, 360*n)
    my_turtle.left(90)
    my_turtle.fd(100)
    my_turtle.left(180)
    my_turtle.end_fill()


def side_list(my_turtle, my_dict, color):
    """List of words aside."""

    my_turtle.shape("turtle")
    goto(my_turtle, 150, 150)
    side_string(my_turtle, my_dict, color)
    my_turtle.shape("classic")


def side_string(my_turtle, my_dict, color):
    """String with info in list aside."""

    n = 0
    for key in my_dict:
        my_turtle.color(color[n])
        n += 1
        my_turtle.shapesize(0.5, 0.5, 1)
        for i in range(20):
            my_turtle.stamp()
            my_turtle.left(18)
        my_turtle.left(360)
        my_turtle.up()
        my_turtle.right(15)
        my_turtle.fd(20)
        my_turtle.write(key + " - " + str(my_dict[key]) + " time(-s)")
        my_turtle.back(20)
        my_turtle.right(75)
        my_turtle.fd(20)
        my_turtle.left(90)
        my_turtle.down()


def sector_dia(my_turtle, my_dict, color):
    """Draw a diagram using sectors."""

    my_list = my_dict.values()
    sum_num = 0
    for i in my_list:
        sum_num += i
    n = 0
    for val in my_dict:
        my_turtle.color(color[n])
        n += 1
        g = my_dict[val]
        grade = g / float(sum_num)
        sector(my_turtle, grade)
    side_list(my_turtle, my_dict, color)


def raw_dia(my_turtle, my_dict, color):
    """Draw a diagram using raws."""

    n = 0
    for val in my_dict:
        my_turtle.color(color[n])
        n += 1
        grade = my_dict[val]
        line(my_turtle, grade, val)
        my_turtle.left(360/len(my_dict))


def main():
    """Main function"""

    liza = turtle.Turtle()
    liza.speed(8)
    wn = turtle.Screen()

    input_string = "My string My little string and another string"
    colors = ["red", "green", "brown", "blue", "purple", "black"]
    my_dict = to_dict(input_string)

    method = int(input("Enter number of diagram type you wish (1 - raws, 2 - sectors): "))
    if method == 1:
        raw_dia(liza, my_dict, colors)
    else:
        sector_dia(liza, my_dict, colors)
    wn.exitonclick()

if __name__ == "__main__":
    main()
