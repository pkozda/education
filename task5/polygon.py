from task5.shape import Shape


class Polygon(Shape):
    """
    Class Polygon. For drawing wright polygons.
    """

    side = 0
    side_len = 0

    def draw(self):
        self.t.begin_fill()
        angle = 360 / self.side
        for i in range(self.side):
            self.t.fd(self.side_len)
            self.t.left(angle)
        self.t.end_fill()

    def get_params(self):
        self.side_len = int(input('Enter length of side: \n'))
        self.side = int(input('Enter number of sides: \n'))
        return self.side_len, self.side

    def set_params(self, params):
        self.side_len = int(params[0])
        self.side = int(params[1])
