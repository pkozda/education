from task5.files_editor import FilesEditor


class NavigationMixin:

    def __init__(self):

        self.opts = {  # All methods with editor and files
            'n': self.create_new,
            'o': self.open_file,
            'r': self.read_file,
            'e': self.edit_file,
            'd': self.delete_file,
            'dr': self.draw_file,
            'cd': self.continue_drawing,
            'cf': self.close_file,
        }

        self.display = {  # Info for view options
            'n': 'Create new',
            'o': 'Open file',
            'r': 'Read file',
            'e': 'Edit file',
            'd': 'Delete file',
            'dr': 'Draw file',
            'cd': 'Draw new shape',
        }

        self.menus = [['n', 'o', 'd'],
                      ['cd', 'r', 'e', 'dr']]  # All menus in one list

        self.state = 0  # Depth of app
        self.file_name = ''  # Name of file for work with
        self.continue_nav = True  # Status of continuation app

        print('Welcome to editor. Are you ready to start?')  # Greetings

    def build_menu(self):
        """ Method for building menu. """

        return self.menus[self.state]

    def get_display_menu(self, menu):
        """ Method for creating view of menu. """

        description = ''
        for option in menu:
            description += self.display[option] + '   '
        return description

    def get_action(self, opt):
        """ Get method to start. """

        return self.opts[opt]

    def check_option(self, opt):
        """ Continue app or stop. """

        if opt != '':
            return opt
        self.continue_nav = False
        return False

    def create_new(self):
        """ Method for creating new file and start new menu. """

        self.file_name = input('Enter name of the document(example.txt): ')
        if FilesEditor.new_file(self.file_name):
            self.state = 1
        else:
            print('File ' + self.file_name + ' already exists. Choose another name.')
            self.create_new()

    def draw(self):
        raise NotImplementedError('Abstract method should be reloaded')

    def read_file(self):
        raise NotImplementedError('Abstract method should be reloaded')

    def edit_file(self):
        raise NotImplementedError('Abstract method should be reloaded')

    def draw_file(self):
        raise NotImplementedError('Abstract method should be reloaded')

    def continue_drawing(self):
        raise NotImplementedError('Abstract method should be reloaded')

    def delete_file(self):
        """ Method for deleting file if it exists. """

        self.file_name = input('Enter name of the document(example.txt): ')
        if FilesEditor.del_file(self.file_name):
            print('Your file ' + self.file_name + ' was deleted!')
        elif self.file_name != '':
            print('File ' + self.file_name + " isn't exists!")
            self.delete_file()

    def open_file(self):
        """ Open file and start new menu to work with it. """

        self.file_name = input('Enter name of your file, please: ')
        FilesEditor.open_file(self.file_name), self.file_name
        print('Your file ' + self.file_name + ' successfully opened. What do you want to do with it? ')
        self.state = 1

    def close_file(self, f):
        """ Method for closing file. """
        raise NotImplementedError('Abstract method should be reloaded')
