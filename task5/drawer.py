from task5.polygon import Polygon
from task5.circle import Circle
import turtle


class Drawer:
    """
    Class for drawing shapes
    """

    shape = ''
    pen = ''
    x_coord = 0
    y_coord = 0
    params = tuple
    shapes = {
        'c': Circle,
        'p': Polygon
    }

    def __init__(self):
        self.wn = turtle.Screen()

    def set_shape(self):
        """ Asks user about shape to draw. """

        print('Enter shape, you want to draw')
        self.shape = input('("c" for Circle or "p" for Polygon):')
        self.shape.strip().lower()

    def set_pen(self):
        """ Asks user about pen color. """

        self.pen = input('Enter color of the pen: ')

    def set_coord(self):
        """ Asks user about coordinates. """

        self.x_coord = int(input('Enter x-coord for start position: '))
        self.y_coord = int(input('Enter y-coord for start position: '))

    def draw(self):
        """ Drawing shape. """

        self.set_shape()
        self.set_pen()
        self.set_coord()
        shape = self.shapes[self.shape](self.pen, self.x_coord, self.y_coord)
        self.params = shape.get_params()
        shape.draw()
        return self.str_to_file()

    def draw_all(self, data):
        """ Drawing all shapes from data. """

        for i in data:
            print(i)
            pen_color = i[1]
            x_coord = int(i[2])
            y_coord = int(i[3])
            params = i[4:]
            shape = self.shapes[i[0][0]](pen_color, x_coord, y_coord)
            shape.set_params(params)
            shape.draw()

    def str_to_file(self):
        """ Preparing string for writing into file. """

        return self.shape + ' ' + self.pen + ' ' + str(self.x_coord) + ' ' + str(self.y_coord) \
                          + ' ' + str(self.params) + '\n'

    def close_wn(self):
        """ Closes canvas. """

        self.wn.reset()
