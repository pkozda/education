from task5.shape import Shape


class Circle(Shape):
    """
    Class Circle. For drawing circles with radius r.
    """

    radius = 0

    def draw(self):
        self.t.begin_fill()
        self.t.circle(self.radius)
        self.t.end_fill()

    def get_params(self):
        self.radius = int(input('Enter radius: \n'))
        return self.radius

    def set_params(self, params):
        self.radius = int(params[0])
