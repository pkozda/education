Задание 5. Фигуры

v.1.4

Нужно спроектировать и реализовать, что-то вроде консольного графического редактора. Т.е. фигуры
создаются и рисуются на нём командами из консоли, а вывод всего документа осуществляется с
помощью модуля Turtle. Редактор должен поддерживать следующие возможности:
- создание нового документа - открытие документа для редактирования - сохранение документа
- добавления фигуры в документ - получение списка фигур - изменение параметров фигуры -
построение изображения с помощью Turtle

start: main.py
