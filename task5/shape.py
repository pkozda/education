import turtle


class Shape:

    pen_col = 'black'
    x_coord = 0
    y_coord = 0

    def __init__(self, pen_col, x, y):
        self.pen_col = pen_col
        self.x_coord = x
        self.y_coord = y
        self.t = turtle.Turtle()
        self.make_turtle()

    def make_turtle(self):
        self.t.fillcolor(self.pen_col)
        self.t.up()
        self.t.goto(self.x_coord, self.y_coord)
        self.t.down()

    def draw(self):
        raise NotImplementedError('Abstract method should be reloaded')

    def get_params(self):
        raise NotImplementedError('Abstract method should be reloaded')

    def set_params(self, params):
        raise NotImplementedError('Abstract method should be reloaded')
