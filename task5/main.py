from task5.editor import Editor


def main():
    """ Main function """

    ed = Editor()
    while ed.continue_nav:
        menu = ed.build_menu()
        print(ed.get_display_menu(menu))
        opt = ed.check_option(input('Enter your choice: '))
        if opt:
            action = ed.get_action(menu[int(opt) - 1])
            action()

if __name__ == '__main__':
    main()
