import os


class FilesEditor:
    """ Class for working with files. """

    @staticmethod
    def new_file(file_name):
        """ Creates new file. """

        if not os.path.exists(file_name):
            f = open(file_name, 'w')
            f.write('')
            f.close()
            return True
        return False

    @staticmethod
    def str_append(file_name, str_to_file):
        """ Writes new data into file. """

        f = open(file_name, 'a')
        f.write(str_to_file)
        f.close()

    @staticmethod
    def read_file(file_name):
        """ Reads your file and returns data from it. """

        f = open(file_name, 'r')
        file_data = []
        for one_line in f:
            file_data.append(one_line.split())
        f.close()
        return file_data

    @staticmethod
    def open_file(file_name):
        """ Opens your file. """

        return open(file_name, 'r')

    @staticmethod
    def close_file(f):
        """ Closes opened file. """

        f.close()

    @staticmethod
    def save_file(file_name, data):
        """ Saves data in file. """

        f = open(file_name, 'w')
        str_data = ''
        for i in data:
            str_data += ' '.join(i) + '\n'
        f.write(str_data)
        f.close()

    @staticmethod
    def del_file(file_name):
        """ Deletes your file if it exists. """

        if os.path.exists(file_name):
            os.remove(file_name)
            return True
        else:
            return False
