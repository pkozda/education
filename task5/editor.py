from task5.drawer import Drawer
from task5.files_editor import FilesEditor
from task5.navigationmixin import NavigationMixin


class Editor(NavigationMixin):
    """
    Main Class
    """

    shape1 = 'Circle'
    shape2 = 'Polygon'

    @staticmethod
    def create_drawer():
        """ Static method which returns object of Drawer() class. """

        return Drawer()

    def draw(self):
        """ Method for drawing several shapes. """

        d = self.create_drawer()
        choice = 'd'
        while choice == 'd':
            FilesEditor.str_append(self.file_name, d.draw())
            choice = input('"d" for draw another shape or "Enter" for Exit: ')

    def read_file(self):
        """
        Method for getting information about data in file.
        :return: data about shapes in file
        """

        data = FilesEditor.read_file(self.file_name)
        for i in data:
            print(i)
        return data

    def close_file(self, f):
        """ Method for closing file. """

        FilesEditor.close_file(f)
        Drawer.close_wn(self.create_drawer())

    def edit_file(self):
        """ Method for edit data in file. """

        data = self.read_file()
        shape_line = int(input('Which shape do you want to change?: '))
        shape_param = int(input('Which param do yoy want to change?: '))
        new_param = input('Enter new param, please: ')
        data[shape_line - 1][shape_param - 1] = new_param
        FilesEditor.save_file(self.file_name, data)
        self.read_file()

    def continue_drawing(self):
        """ Method for continue drawing from point where were stop. """

        Drawer.close_wn(self.create_drawer())
        self.draw_file()
        self.draw()

    def draw_file(self):
        """ Method for drawing all shapes from file. """

        Drawer.close_wn(self.create_drawer())
        data = FilesEditor.read_file(self.file_name)
        d = self.create_drawer()
        print('drawing...')
        d.draw_all(data)

    def __str__(self):
        return self.shape1 + ', ' + self.shape2
