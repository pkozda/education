import random


def get_rand():
    """Returns random number between 1 and 9"""

    n = random.randrange(1, 10)
    return n


def get_pyramid(grade):
    """Get a pyramid with numbers"""

    pyramid = []
    for i in range(grade):
        raw = []
        for k in range(i+1):
            raw = raw + [get_rand()]
        pyramid.append(raw)
        print raw
    return pyramid


def get_sum_pyr2(pyramid):
    """Returns max sum using dynamic method"""

    for row in range(len(pyramid) - 2, -1, -1):
        for col in range(row + 1):
            pyramid[row][col] += max(pyramid[row + 1][col], pyramid[row + 1][col + 1])
    return pyramid[0][0]


def get_sum_pyr(pyramid, row, col, sum_):
    """Returns max sum using recursive method. Not works yet ((("""

    if row == len(pyramid) - 1:
        return sum_ + pyramid[row][col]
    sum_ = sum_ + pyramid[row][col]
    sum1 = sum_ + pyramid[row+1][col]
    sum2 = sum_ + pyramid[row+1][col+1]
    if sum1 > sum2:
        sum_ = sum1
    else:
        sum_ = sum2
    return get_sum_pyr(pyramid, row + 1, col + 1, sum_)


def main():
    """Main function"""

    grade = 8
    pyramid = get_pyramid(grade)

    new_pyramid = pyramid[:]
    sum_pyr = get_sum_pyr2(new_pyramid)

    print "Max sum of this pyramid =", sum_pyr

if __name__ == "__main__":
    main()
