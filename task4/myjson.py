import json


class Myjson:

    def __init__(self, data):
        self.file_name = data[0]  # name of file with json string
        self.data = data[1]  # python data to convert

    def serialize(self):
        """ Method for convert python data into json string. """

        f = open(self.file_name, 'w')
        f.write(json.dumps(self.data))
        f.close()
        return self.file_name

    @staticmethod
    def unserialize(file_name):
        """ Method for convert json string into python data. """

        f = open(file_name, 'r')
        data = json.loads(f.read())
        f.close()
        return data
