import random
from coeff import Coeff
from conversions import Conversions


class Points:

    def __init__(self, settings):
        self.x_min = -1.5  # min x coords (depends on resolution)
        self.x_max = 1.5  # max x coords
        self.y_min = -1  # min y coords
        self.y_max = 1  # max y coords

        self.x_res = int(settings[0])  # resolution of image(width)
        self.y_res = int(settings[1])  # resolution of image(height)

        self.conversion = settings[2]  # conversion to use (lined, or several non-lined)
        self.affines = settings[3]  # number of affine coeffs
        self.num_points = settings[4]  # number of points to draw
        self.iterations = settings[5]  # number of iterations per point

        self.points = {}  # all points that will be drawing
        self.seed = random.randrange(0, 1000)  # random seed for random generator (can be changed)
        self.affine_coeffs = Coeff.get_data(self.affines)  # list of affine coeffs

    def set_seed(self, seed):
        """ Set seed for random generator. """
        self.seed = seed

    def set_affine_coeffs(self, affine_coeffs):
        """ Set affines coeffs. """
        self.affine_coeffs = affine_coeffs

    @classmethod
    def get_random_affine(cls, data):
        """
        :return: random affine from affine list.
        """
        num = random.randrange(0, len(data))
        return data[num]

    def get_conversion(self, x, y):
        """
        :return: lined or non-lined conversion of x and y
        """
        c = Conversions(x, y, self.conversion)
        return c.get_conversion()   # Conversions(x, y, self.conversion)

    def render(self):
        """
        General method of class
        :return: generates list of points to draw on canvas.
        """
        random.seed(self.seed)  # set seed for random generator

        for one_line in range(self.num_points):

            if one_line == self.num_points / 4:
                print('25%...')
            elif one_line == self.num_points / 2:
                print('50%...')
            elif one_line == self.num_points / 4 * 3:
                print('75%...')
            elif one_line == self.num_points / 100 * 95:
                print('Almost done...')

            new_x = random.uniform(self.x_min, self.x_max)
            new_y = random.uniform(self.y_min, self.y_max)

            for iteration in range(self.iterations):
                affine = self.get_random_affine(self.affine_coeffs)  # Getting random affine and using it
                x = affine[0] * new_x + affine[1] * new_y + affine[2]
                y = affine[3] * new_x + affine[4] * new_y + affine[5]

                new_x, new_y = self.get_conversion(x, y)  # Getting conversion
                if iteration > 20 and self.x_min < new_x < self.x_max and self.y_min < new_y < self.y_max:
                    # Getting coords of point
                    x1 = self.x_res - int((self.x_max - new_x) / (self.x_max - self.x_min) * self.x_res)
                    y1 = self.y_res - int((self.y_max - new_y) / (self.y_max - self.y_min) * self.y_res)
                    if x1 < self.x_res and y1 < self.y_res:  # if point in our resolution
                        coords = x1, y1
                        if coords not in self.points:
                            self.points[coords] = affine[6]
                        else:
                            self.points[coords] = [int((affine[6][0] + self.points[coords][0]) / 2),
                                                   int((affine[6][1] + self.points[coords][1]) / 2),
                                                   int((affine[6][2] + self.points[coords][2]) / 2)]
        return self.points, self.seed, self.affine_coeffs
