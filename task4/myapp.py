import os
import redis
from werkzeug.wrappers import Request, Response
from werkzeug.routing import Map, Rule
from werkzeug.exceptions import HTTPException, NotFound
from werkzeug.wsgi import SharedDataMiddleware
from werkzeug.utils import redirect
from jinja2 import Environment, FileSystemLoader
from points import Points
from drawer import Drawer


def is_valid_form(settings):
        return True


class Myapp(object):

    def __init__(self, config):
        self.redis = redis.Redis(config['redis_host'], config['redis_port'])
        template_path = os.path.join(os.path.dirname(__file__), 'templates')
        self.jinja_env = Environment(loader=FileSystemLoader(template_path),
                                     autoescape=True)
        self.url_map = Map([
            Rule('/', endpoint='new_fractal'),
            Rule('/fractal', endpoint='draw_link')
        ])

    def render_template(self, template_name, **context):
        t = self.jinja_env.get_template(template_name)
        return Response(t.render(context), mimetype='text/html')

    def on_new_fractal(self, request):
        error = None
        if request.method == 'POST':
            width = request.form['width']
            height = request.form['height']
            conversion = request.form['conversion']
            affines = int(request.form['affines'])
            num_points = int(request.form['num_points'])
            iterations = int(request.form['iter'])
            settings = [width, height, conversion, affines, num_points, iterations]
            if not is_valid_form(settings):
                error = 'Please enter a valid form data'
            else:
                p = Points(settings)
                print('Stand by...')
                data = p.render()
                print('Drawing...')
                Drawer(data[0])
                return redirect('/%s' % 'fractal')
        return self.render_template('new_fractal.html', error=error)

    def on_draw_link(self, request):
        img = os.path.join(os.path.dirname(__file__), 'flames.png')
        return self.render_template('fractal.html', img=img)

    def dispatch_request(self, request):
        adapter = self.url_map.bind_to_environ(request.environ)
        try:
            endpoint, values = adapter.match()
            return getattr(self, 'on_' + endpoint)(request, **values)
        except HTTPException, e:
            return e

    def wsgi_app(self, environ, start_response):
        request = Request(environ)
        response = self.dispatch_request(request)
        return response(environ, start_response)

    def __call__(self, environ, start_response):
        return self.wsgi_app(environ, start_response)


def create_app(redis_host='localhost', redis_port=6379, with_static=True):
    app = Myapp({
        'redis_host':       redis_host,
        'redis_port':       redis_port
    })
    if with_static:
        app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
            '/static':  os.path.join(os.path.dirname(__file__), 'static')
        })
    return app

if __name__ == '__main__':
    from werkzeug.serving import run_simple
    my_app = create_app()
    run_simple('127.0.0.1', 5000, my_app, use_debugger=True, use_reloader=True)
