import math


def sqrt(a, b):
    """ Function for help. Returns square root of sum squares """
    return math.sqrt(a ** 2 + b ** 2)


class Conversions:
    """ Class for choosing non-lined functions. """

    def __init__(self, x, y, name):
        self.x = x
        self.y = y
        self.name = name
        self.names = {
            'l': self.lined,
            'si': self.sinusoidal,
            'sp': self.spherical,
            'p': self.polar,
            'h': self.heart,
            'd': self.disc,
        }

    def get_conversion(self):
        return self.names[self.name]()

    def lined(self):
        """ Lined conversion """

        return self.x, self.y

    def sinusoidal(self):
        """ Sinusoidal affine conversion """

        return math.sin(self.x), math.sin(self.y)

    def spherical(self):
        """ spherical affine conversion """

        return (self.x / (self.x ** 2 + self.y ** 2),
                self.y / (self.x ** 2 + self.y ** 2))

    def polar(self):
        """ Polar affine conversion """

        return math.atan(self.y / self.x) / math.pi, sqrt(self.x, self.y) - 1

    def heart(self):
        """ Heart-shaped affine conversion """

        return (sqrt(self.x, self.y) * math.sin(sqrt(self.x, self.y)) * math.atan(self.y / self.x),
                -sqrt(self.x, self.y) * math.cos(sqrt(self.x, self.y) * math.atan(self.y / self.x)))

    def disc(self):
        """ Disc affine conversion """

        return (1 / math.pi * math.atan(self.y / self.x) * math.sin(math.pi * sqrt(self.x, self.y)),
                1 / math.pi * math.atan(self.y / self.x) * math.cos(math.pi * sqrt(self.x, self.y)))
