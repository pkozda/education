import random


class Coeff:
    """ Class for generating affine """

    def __init__(self):
        self.a = random.uniform(-1, 1)
        self.b = random.uniform(-1, 1)
        self.d = random.uniform(-1, 1)
        self.e = random.uniform(-1, 1)

        self.c = random.uniform(-1.5, 1.5)
        self.f = random.uniform(-1.5, 1.5)

        self.R = random.randint(0, 255)
        self.G = random.randint(0, 255)
        self.B = random.randint(0, 255)

    def conditions(self):
        """
        :return: True if all conditions will be passed
        """

        if self.a ** 2 + self.d ** 2 < 1 and self.b ** 2 + self.e ** 2 < 1 and \
           self.a ** 2 + self.b ** 2 + self.d ** 2 + self.e ** 2 < \
           1 + (self.a * self.e - self.b * self.d) ** 2:
                return True
        return False

    def to_list(self, new_obj):
        """
        :return: List with a,b,c,d,e,f coefficients and RGB color
        """

        while not new_obj.conditions():
            return self.to_list(Coeff())
        else:
            return [self.a, self.b, self.c, self.d, self.e, self.d,
                   [self.R, self.G, self.B]]

    @staticmethod
    def get_data(n):
        """
        :param n: number of affines we need
        :return: data with affines
        """

        data = []
        for i in range(n):
            data.append(Coeff.to_list(Coeff(), Coeff()))
        return data
