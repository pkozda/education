from points import Points
from drawer import Drawer
from myjson import Myjson


def load():
    print('Enter, please, the name of file with your data')
    file_name = input()

    data = Myjson.unserialize(file_name)
    p = Points(data[0])
    p.set_seed(data[1])
    p.set_affine_coeffs(data[2])

    print('Stand by...')
    data = p.render()

    print('Drawing...')
    Drawer(data[0])


def new():
    print('Hola, are you ready to draw something? I need some info to make what you want.')
    print('Enter file name where will be saved all data')
    file_name = input()

    print('Enter, please resolution of picture you want. First enter width')
    width = input()

    print('Now enter, please, height')
    height = input()

    print('Now i need to know what kind of affine conversion do you want.')
    print('We have "lined", "sinusoidal", "spherical", "polar", "heart" and "disc".')
    print('Enter only first letter(in case "s" type second letter too)')
    conversion = input()

    print('How do you think, how many affines should I get?')
    affines = int(input())

    print('And what about points. How many?')
    num_points = int(input())

    print('And now I need to know, how deep your image should be. Enter number of iterations')
    iterations = int(input())

    settings = [width, height, conversion, affines, num_points, iterations]
    p = Points(settings)
    print('Stand by...')
    data = p.render()

    print('Drawing...')
    Drawer(data[0])

    json_data = (file_name, [settings, data[1], data[2]])
    j = Myjson(json_data)
    file_name = j.serialize()
    print('All your data from now will be in file:', file_name, 'You can load it whenever you want.')


def main():
    print('Hello, do you want load file or draw new? ("load", "new")')
    choice = input()
    if choice == 'new':
        new()
    elif choice == 'load':
        load()

if __name__ == '__main__':
    main()
