from PIL import Image, ImageDraw


class Drawer:
    """ Class for drawing image. """

    def __init__(self, data):
        self.data = data
        self.color = (0, 0, 0)  # Background color for your image
        self.size = (800, 600)  # Size of the image
        self.name = 'flames.png'  # Name of file where to save your image
        self.start_drawing()

    def start_drawing(self):
        img = Image.new('RGB', self.size, self.color)
        img_drawer = ImageDraw.Draw(img)

        for x in range(len(self.data)):
            for y in range(len(self.data[x])):
                r = self.data[x][y][1]
                g = self.data[x][y][2]
                b = self.data[x][y][3]
                img_drawer.point((x, y), (r, g, b))  # drawing every point
        img.save(self.name)
